<jsp:useBean id="preds" 
			 type="com.carm.wsur.predictions.Predictions" 
			 class="com.carm.wsur.predictions.Predictions">
	<% 
		// Check the HTTP verb: if it's anything but GET,
		// return 405 (Method Not Allowed).
		String verb = request.getMethod();

     	if (!verb.equalsIgnoreCase("GET")) {
			response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "GET requests only are allowed.");
		}
     	// If it's a GET request, return the predictions.
     	else {
	       	// Object reference application has the value 
	       	// pageContext.getServletContext()
       		preds.setServletContext(application);
       		out.println(preds.getPredictions());
     	}
	%>
</jsp:useBean>