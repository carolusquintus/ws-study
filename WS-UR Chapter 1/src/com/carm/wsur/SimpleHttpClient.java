package com.carm.wsur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

public class SimpleHttpClient {

	public static void main(String[] args) {
		// Usage
		if (args.length < 1) {
			System.err.println("Usage: SimpleHttpClient <url>");
			return;
		}

		try {
			// Parse the URL
			URL url = new URL(args[0]);
			String host = url.getHost();
			String path = url.getPath();
			int port = url.getPort();

			if (port < 0)
				port = 80;

			// Send the request
			String request = "GET " + path + " HTTP/1.1\n";
			request += "host: " + host;
			request += "\n\n";
			Socket sock = new Socket(host, port);
			PrintWriter writer = new PrintWriter(sock.getOutputStream());
			writer.print(request);
			writer.flush();

			// Read and print the response
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					sock.getInputStream()));

			String nextRecord = null;

			while ((nextRecord = reader.readLine()) != null)
				System.out.println(nextRecord);

			sock.close();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}