package com.carm.wsur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Date: Sep 30, 2014
 *
 * @author carolus
 */
public class UrlConnectionClient {

    public static void main(String[] args) {
        // Usage
        if (args.length < 1) {
            System.err.println("Usage: UrlConnectionClient <url>");
            return;
        }

        try {
            // Parse the URL
            URL url = new URL(args[0].trim());

            // Connect
            URLConnection sock = url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));

            String nextRecord = null;

            while ((nextRecord = reader.readLine()) != null)
                System.out.println(nextRecord);

            reader.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
